import { Injectable } from "@angular/core";
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  CanLoad,
  Route,
  UrlSegment,
  Router,
} from "@angular/router";
import { Observable, of } from "rxjs";
import { AuthenticateService } from "./authenticate.service";

@Injectable({
  providedIn: "root",
})
export class AuthGuard implements CanLoad {
  constructor(private router: Router, private authService: AuthenticateService) {}

  canLoad(
    route: Route,
    segments: UrlSegment[]
  ):  Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (!this.authService.isAuthenticate()) {
      this.router.navigateByUrl("/auth");
    }
    return this.authService.isAuthenticate();
  }
}
