import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { AuthenticateService } from './authenticate.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.page.html',
  styleUrls: ['./auth.page.scss'],
})
export class AuthPage implements OnInit {
public isLoading=false;
  constructor(private router:Router, private authenticateService:AuthenticateService, public loadCtrl:LoadingController) { }

  ngOnInit() {
  }
  onLogin() {
    this.authenticateService.doLogin();
    this.isLoading = true;
    this.loadCtrl.create({keyboardClose:true, message:' Loading...'}).then(loadingEl => {
      loadingEl.present();

      setTimeout(() => {
        this.router.navigateByUrl('/places/offers');
        loadingEl.dismiss();
        this.isLoading = false;
      }, 1000);
    })
    

    
  }
}
