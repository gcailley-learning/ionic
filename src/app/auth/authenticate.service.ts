import { Injectable } from "@angular/core";

@Injectable({providedIn:'root'})
export class AuthenticateService {

  private _userIsAuthenticate: boolean;
  constructor() {
    this._userIsAuthenticate = false;
  }
  doLogin() {
    this._userIsAuthenticate = true;
    console.log('Is logged In');
  }
  doLogout() {
    this._userIsAuthenticate = false;
    console.log('Is logged Out');
  }
  isAuthenticate(): boolean | import("@angular/router").UrlTree | import("rxjs").Observable<boolean | import("@angular/router").UrlTree> | Promise<boolean | import("@angular/router").UrlTree> {
   return this._userIsAuthenticate;
  }
}
