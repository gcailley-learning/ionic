import { Injectable } from "@angular/core";
import { Booking } from "./booking.model";

@Injectable({ providedIn: "root" })
export class BookingService {
  private _bookings: Booking[] = [new Booking("1", "p1", "u1", "GreatePlace", 2)];
  constructor() {
    for (let index = 0; index < 200; index++) {
      this._bookings.push(new Booking('' + index, "p" + index, "u1", "GreatePlace" + index, index%4));
    }
  }

  get bookings() {
    return [...this._bookings];
  }
}
