import { Component, Input, OnInit } from "@angular/core";
import { IonInput, ModalController } from "@ionic/angular";
import { Place } from "src/app/places/pages.model";

@Component({
  selector: "app-create-booking",
  templateUrl: "./create-booking.component.html",
  styleUrls: ["./create-booking.component.scss"],
})
export class CreateBookingComponent implements OnInit {
  @Input() selectedPlace: Place;
  constructor(private modalCtrl: ModalController) {}

  ngOnInit() {}

  onCancel() {
    this.modalCtrl.dismiss(null, 'cancel');
  }

  onBookPlace() {
    this.modalCtrl.dismiss({ message: "book ", place: this.selectedPlace }, 'booked');
  }
}
