import { Component, OnInit } from '@angular/core';
import { IonSegment } from '@ionic/angular';
import { PlacesServices } from '../paces.services';
import { Place } from '../pages.model';

@Component({
  selector: 'app-discover',
  templateUrl: './discover.page.html',
  styleUrls: ['./discover.page.scss'],
})
export class DiscoverPage implements OnInit {
  loadedPlaces : Place[] = [];
  constructor(private placesService: PlacesServices) { }

  ngOnInit() {
    this.loadedPlaces = this.placesService.places;
    console.log(this.loadedPlaces);
  }
  onChangeFilter(event:CustomEvent<IonSegment>) {
    console.log(event.detail);

  }
}
