import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateBookingComponent } from 'src/app/bookings/create-booking/create-booking.component';

import { PlaceDetailPage } from './place-detail.page';

const routes: Routes = [
  {
    path: '',
    component: PlaceDetailPage
  }
];

@NgModule({
  declarations:[CreateBookingComponent],
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PlaceDetailPageRoutingModule {}
