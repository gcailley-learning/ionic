import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ActionSheetController, ModalController, NavController } from "@ionic/angular";
import { CreateBookingComponent } from "src/app/bookings/create-booking/create-booking.component";
import { PlacesServices } from "../../paces.services";
import { Place } from "../../pages.model";

@Component({
  selector: "app-place-detail",
  templateUrl: "./place-detail.page.html",
  styleUrls: ["./place-detail.page.scss"],
})
export class PlaceDetailPage implements OnInit {
  public place: Place;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private _navCtrl: NavController,
    private placesService: PlacesServices,
    private modalCtrl: ModalController,
    private actionSheetCtrl: ActionSheetController
  ) {}

  ngOnInit() {
    this.route.paramMap.subscribe((paramsMap) => {
      if (paramsMap.has("placeId")) {
        this.place = this.placesService.getPlaces(paramsMap.get("placeId"));
      } else {
        this.router.navigateByUrl("/places/discover");
        return;
      }
    });
  }

  onBookPlace() {
    console.log('onBookPlace');
    //     this._router.navigate(["/places/discover"]);
    // this._navCtrl.navigateBack("/places/discover");
    this.actionSheetCtrl.create({
      header: "Choose Action",
      buttons: [
        {
          text: "Select Date",
          handler: () => {
            this.openBookingModal("select");
          },
        },

        {
          text: "Random Date",
          handler: () => {
            this.openBookingModal("random");
          },
        },

        {
          text: "Cancel",
          role: "cancel",
        },
      ],
    }).then( sheetEl => {
      sheetEl.present();
    });
  }

  openBookingModal(action: "select" | "random") {
    console.log(action);
    this.modalCtrl
      .create({
        component: CreateBookingComponent,
        componentProps: { selectedPlace: this.place },
      })
      .then((modalEl) => {
        modalEl.present();
        return modalEl.onDidDismiss();
      })
      .then((resultatData: any) => {
        if (resultatData.role === "booked") {
          console.log(resultatData.data);
        } else {
          console.log("Cancelled");
        }
      });
  }
}
