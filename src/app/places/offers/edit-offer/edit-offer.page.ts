import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { NavController } from "@ionic/angular";
import { PlacesServices } from "../../paces.services";
import { Place } from "../../pages.model";

@Component({
  selector: "app-edit-offer",
  templateUrl: "./edit-offer.page.html",
  styleUrls: ["./edit-offer.page.scss"],
})
export class EditOfferPage implements OnInit {
  public place: Place;

  constructor(private route: ActivatedRoute, private navCtrl: NavController, private placesService: PlacesServices) {}

  ngOnInit() {
    this.route.paramMap.subscribe((paramsMap) => {
      if (paramsMap.has("placeId")) {
        this.place = this.placesService.getPlaces(paramsMap.get("placeId"));
      } else {
        this.navCtrl.navigateBack("/places/offers");
        return;
      }
    });
  }
}
