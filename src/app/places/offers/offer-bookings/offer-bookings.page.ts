import { Route } from "@angular/compiler/src/core";
import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { NavController } from "@ionic/angular";
import { PlacesServices } from "../../paces.services";
import { Place } from "../../pages.model";

@Component({
  selector: "app-offer-bookings",
  templateUrl: "./offer-bookings.page.html",
  styleUrls: ["./offer-bookings.page.scss"],
})
export class OfferBookingsPage implements OnInit {
  public place: Place;

  constructor(private route: ActivatedRoute, private navCtrl: NavController, private placesService: PlacesServices) {}

  ngOnInit() {
    this.route.paramMap.subscribe((paramsMap) => {
       if (paramsMap.has("placeId")) {
        this.place = this.placesService.getPlaces(paramsMap.get("placeId"));
      } else {
        this.navCtrl.navigateBack("/places/offers");
        return;
      }
    });
  }
}
