import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { IonItemSliding, MenuController, NavController } from "@ionic/angular";
import { off } from "process";
import { PlacesServices } from "../paces.services";
import { Place } from "../pages.model";

@Component({
  selector: "app-offers",
  templateUrl: "./offers.page.html",
  styleUrls: ["./offers.page.scss"],
})
export class OffersPage implements OnInit {
  public offers: Place[];
  constructor(private _placesService: PlacesServices, private menuCtl: MenuController, private router: Router) {}

  ngOnInit() {
    this.offers = this._placesService.places;
  }

  onOpenMenu() {
    console.log("onOpenMenu");
    this.menuCtl.get("m2").then((menu) => {
      console.log("Menu", menu);
      menu.open();
    });
  }

  onEdit(offerId: string, slidingItem: IonItemSliding) {
    slidingItem.close();
    console.log(offerId);
    this.router.navigateByUrl("/places/offers/" + offerId);
  }
}
