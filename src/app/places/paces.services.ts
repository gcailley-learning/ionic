import { Injectable } from "@angular/core";
import { Place } from "./pages.model";

@Injectable({providedIn:'root'}) 
export class PlacesServices  {
    
    private _places: Place[] = [
        new Place ('p1', 'M', 'xx', 'https://img.static-af.com/images/meta/IDname/CITY-MRU-1?aspect_ratio=2:1&max_width=1920', 200), 
        new Place ('p2', 'Q', 'qx', 'https://img.static-af.com/images/meta/IDname/CITY-MRU-1?aspect_ratio=2:1&max_width=1920', 200),
        new Place ('p3', 'X', 'fx', 'https://img.static-af.com/images/meta/IDname/CITY-MRU-1?aspect_ratio=2:1&max_width=1920', 200),
    ];

    constructor() {

    }

    get places () {
        return [...this._places];
    }

    getPlaces(placeId: any): Place {
       return {...this._places.find(p => p.id = placeId)};
      }
}